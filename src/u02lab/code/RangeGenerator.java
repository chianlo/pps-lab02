package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private List<Optional<Integer>> range;
    private int start;
    private int stop;
    private int index;

    public RangeGenerator(int start, int stop){
        this.range = new ArrayList<>();
        this.start = start;
        this.stop = stop;
        this.index = start -1;
        for(int i = start; i < stop; i++) {
            this.range.add(Optional.ofNullable(i));
        }
        this.range.add(Optional.empty());
    }

    @Override
    public Optional<Integer> next() {
        return this.range.get(index++);
    }

    @Override
    public void reset() {
        this.index = this.start - 1;
    }

    @Override
    public boolean isOver() {
        return this.index == this.stop -1;
    }

    @Override
    public List<Integer> allRemaining() {
        return null;
    }
}
