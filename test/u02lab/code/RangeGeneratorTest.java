package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RangeGeneratorTest {

    private static final int START = 1;
    private static final int STOP = 10;
    private RangeGenerator rangeGenerator;

    @Before
    public void setUp() {
        this.rangeGenerator = new RangeGenerator(START, STOP);
    }

    @Test
    public void elementsAreProduced() {
        for (int i = START; i < STOP; i++) {
            assertTrue (this.rangeGenerator.next().equals(Optional.of(i)));
        }
    }

    @Test
    public void callNextAfterStop() {
        for (int i = START; i < STOP; i++) {
            this.rangeGenerator.next();
        }
        assertTrue (this.rangeGenerator.next().equals(Optional.empty()));
    }

    @Test
    public void isResetted() {
        for (int i = START; i < STOP; i++) {
            this.rangeGenerator.next();
        }
        this.rangeGenerator.reset();
        assertTrue(this.rangeGenerator.next().equals(Optional.of(START)));
    }

    @Test
    public void isOver() {
        for (int i = START; i < STOP/2; i++) {
            this.rangeGenerator.next();
        }
        assertFalse(this.rangeGenerator.isOver());
        this.rangeGenerator.reset();
        for (int i = START; i < STOP; i++) {
            this.rangeGenerator.next();
        }
        assertTrue(this.rangeGenerator.isOver());
    }

    @Test
    public void checkRemainingList() {
        int newStop = STOP/2;
        for (int i = START; i < newStop; i++) {
            this.rangeGenerator.next();
        }
        List<Integer> remainingList = this.rangeGenerator.allRemaining();
        List<Integer> checkRemainingist = new ArrayList<>();
        for (int i = newStop; i < STOP; i++) {
            checkRemainingist.add(this.rangeGenerator.next().get());
        }
        assertTrue(remainingList.equals(checkRemainingist));
    }
}


